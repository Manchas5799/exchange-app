/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

ALTER TABLE `general_settings`
ADD COLUMN `p2p` longtext COMMENT '';
UPDATE `general_settings` SET `p2p` = '\"{\\\"network_fee\\\":\\\"1\\\",\\\"application_wallet\\\":\\\"USDT\\\",\\\"application_balance\\\":\\\"1000\\\"}\"' WHERE `id` = 1;

UPDATE `extensions` SET `dev` = 0 WHERE `id` = 8;

UPDATE `permissions` SET `title` = 'Delete Peer to Peer Entries', `code` = 'p2p_delete' WHERE `id` = 161;

UPDATE `permissions` SET `title` = 'Moderate Peer to Peer Orders', `code` = 'p2p_moderate' WHERE `id` = 164;

UPDATE `permissions` SET `title` = 'Edit Peer to Peer Entries', `code` = 'p2p_edit' WHERE `id` = 165;

INSERT INTO `permissions` (`title`, `code`, `tab`, `created_at`, `updated_at`) VALUES
('Access To Ecommerce ', 'ecommerce_access', 'addons', NULL, NULL);

INSERT INTO `permissions` (`title`, `code`, `tab`, `created_at`, `updated_at`) VALUES
('Delete Entries In Ecommerce ', 'ecommerce_delete', 'addons', NULL, NULL);

INSERT INTO `permissions` (`title`, `code`, `tab`, `created_at`, `updated_at`) VALUES
('Edit Entries In Ecommerce ', 'ecommerce_edit', 'addons', '2021-12-03 15:00:00', '2021-12-04 15:00:00');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;