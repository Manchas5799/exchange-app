/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

UPDATE `notification_templates` SET `shortcodes` = '{\"amount\":\"Amount\",\"currency\":\"Currency\",\"trx\":\"Transaction Code\",\"post_balance\":\"Post Balance\",\"charge\":\"Charge\",\"rate\":\"Rate\",\"method_currency\":\"Withdraw Method Currency\",\"method_name\":\"Withdraw Method Name\",\"method_amount\":\"Withdraw Method Amount\",\"admin_details\":\"Admin Remark\"}' WHERE `id` = 38;

UPDATE `notification_templates` SET `shortcodes` = '{\"amount\":\"Amount\",\"currency\":\"Currency\",\"trx\":\"Transaction Code\",\"post_balance\":\"Post Balance\",\"charge\":\"Charge\",\"rate\":\"Rate\",\"method_currency\":\"Withdraw Method Currency\",\"method_name\":\"Withdraw Method Name\",\"method_amount\":\"Withdraw Method Amount\",\"delay\":\"Delay\"}' WHERE `id` = 37;

UPDATE `notification_templates` SET `shortcodes` = '{,\"amount\":\"Amount\",\"currency\":\"Currency\",\"trx\":\"Transaction Code\",\"post_balance\":\"Post Balance\",\"charge\":\"Charge\",\"rate\":\"Rate\",\"method_currency\":\"Withdraw Method Currency\",\"method_name\":\"Withdraw Method Name\",\"method_amount\":\"Withdraw Method Amount\",\"admin_details\":\"Admin Remark\"}' WHERE `id` = 36;

INSERT INTO `notification_templates` (`id`, `act`, `name`, `subj`, `email_body`, `sms_body`, `push_notification_body`, `shortcodes`, `email_status`, `sms_status`, `push_notification_status`, `created_at`, `updated_at`) VALUES
(46, 'PROVIDER_WITHDRAW', 'Withdraw - Successfull', 'Withdraw Request has been Processed and your money is sent - {{site_name}}', 'Your withdraw request of {{amount}} {{currency}} to {{recieving_address}} has been Processed Successfully.\r\n<br><br>\r\nDetails of your withdraw:\r\n<br><br>\r\nAmount : {{amount}} {{currency}}<br>\r\nCharge: {{charge}} {{currency}}\r\n<br><br>\r\nYou will get: {{recieved}} {{currency}}\r\n<br><br>\r\nTransaction Number : {{trx}}\r\n<br><br>', 'Your withdraw request of {{amount}} {{currency}} to {{recieving_address}} has been Processed Successfully.', 'Your withdraw request of {{amount}} {{currency}} to {{recieving_address}} has been Processed Successfully.', '{\"amount\":\"Amount\",\"currency\":\"Currency\",\"trx\":\"Transaction Code\",\"post_balance\":\"Post Balance\",\"charge\":\"Fees\"\"recieved\":\"Recieved Amount\",\"recieving_address\":\"Client Recieving Address\"}', 1, 1, 1, NULL, NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;