/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Check if a record with the specified `id` exists and insert if it doesn't
INSERT INTO `notification_templates` (
  `id`, `act`, `name`, `subj`, `email_body`, `sms_body`, `push_notification_body`, `shortcodes`,
  `email_status`, `sms_status`, `push_notification_status`, `created_at`, `updated_at`
)
SELECT 
  48, 'USER_LOGIN_NOTIFICATION', 'User Login Notification', 'New Login Detected - {{site_name}}',
  'A new login has been detected on your account.\r\n<br><br>\r\nDetails:\r\n<br><br>\r\nIP Address: {{ip_address}}<br>\r\nLocation: {{location}}<br>\r\nDate and Time: {{login_time}}\r\n<br><br>\r\nIf you recognize this login, no further action is required. If not, please contact support immediately.',
  'A new login has been detected on your account from IP: {{ip_address}}. If unrecognized, please contact support.',
  'A new login has been detected on your account from IP: {{ip_address}}. If unrecognized, please contact support.',
  '{"ip_address":"IP Address","location":"Location","login_time":"Date and Time"}',
  1, 1, 1, NULL, NULL
FROM DUAL
WHERE NOT EXISTS (SELECT 1 FROM `notification_templates` WHERE `id` = 48);

INSERT INTO `notification_templates` (
  `id`, `act`, `name`, `subj`, `email_body`, `sms_body`, `push_notification_body`, `shortcodes`,
  `email_status`, `sms_status`, `push_notification_status`, `created_at`, `updated_at`
)
SELECT
  49, 'PRODUCT_PURCHASE_SUCCESS', 'Product Purchase - Successful', 'Product Purchase Completed Successfully - {{site_name}}',
  'Congratulations! You have successfully purchased the product {{product_name}} from the category {{category_name}}.\r\n<br><br>\r\nDetails of your purchase:\r\n<br><br>\r\nAmount: {{amount}} {{currency}} <br>\r\nDiscount: {{discount}}% <br>\r\nTransaction Number: {{trx}}\r\n<br><br>\r\nYour current Balance is {{post_balance}} {{currency}}',
  'You have successfully purchased the product {{product_name}} from the category {{category_name}} for {{amount}} {{currency}}.',
  'You have successfully purchased the product {{product_name}} from the category {{category_name}} for {{amount}} {{currency}}.',
  '{\"product_name\":\"Product Name\",\"category_name\":\"Category Name\",\"product_id\":\"Product ID\",\"amount\":\"Amount\",\"currency\":\"Currency\",\"discount\":\"Discount\",\"trx\":\"Transaction Code\",\"post_balance\":\"Post Balance\"}',
  1, 1, 1, NULL, NULL
FROM
  (SELECT 1) AS dummy
WHERE
  NOT EXISTS (SELECT 1 FROM `notification_templates` WHERE `id` = 49);


UPDATE `notification_templates` SET `email_body` = '<p>Your deposit request has been approved!</p>\r\n\r\n            <h3>Deposit Details:</h3>\r\n            <table>\r\n                <tr>\r\n                    <td>Amount:</td>\r\n                    <td>{{amount}} {{method_currency}}</td>\r\n                </tr>\r\n                <tr>\r\n                    <td>Charge:</td>\r\n                    <td>{{charge}} {{method_currency}}</td>\r\n                </tr>\r\n                <tr>\r\n                    <td>Conversion Rate:</td>\r\n                    <td>1 {{currency}} = {{rate}} {{method_currency}}</td>\r\n                </tr>\r\n                <tr>\r\n                    <td>Payable:</td>\r\n                    <td>{{method_amount}} {{method_currency}}</td>\r\n                </tr>\r\n                <tr>\r\n                    <td>Payment Method:</td>\r\n                    <td>{{method_name}}</td>\r\n                </tr>\r\n                <tr>\r\n                    <td>Transaction Number:</td>\r\n                    <td>{{trx}}</td>\r\n                </tr>\r\n            </table>\r\n\r\n            <p>Your current balance is {{post_balance}} {{currency}}.</p>' WHERE `id` = 34;
UPDATE `notification_templates` SET `email_body` = 'Your deposit request of {{amount}} {{currency}} is via  {{method_name}} submitted successfully.\r\n<br><br>\r\nDetails of your Deposit :\r\n<br><br>\r\nAmount : {{amount}} {{currency}}<br>\r\nCharge: {{charge}} {{method_currency}}\r\n<br><br>\r\nConversion Rate : 1 {{currency}} = {{rate}} {{method_currency}}<br>\r\nPayable : {{method_amount}} {{method_currency}}<br>\r\nPay via :  {{method_name}}\r\n<br><br>\r\nTransaction Number : {{trx}}' WHERE `id` = 33;
UPDATE `notification_templates` SET `email_body` = 'Your deposit of {{amount}} {{currency}} is via  {{method_name}} has been completed Successfully.\r\n<br><br>\r\nDetails of your Deposit :\r\n<br><br>\r\nAmount : {{amount}} {{currency}}<br>\r\nCharge: {{charge}} {{method_currency}}\r\n<br><br>\r\nTransaction Number : {{trx}}\r\n<br><br>\r\nYour current Balance is {{post_balance}} {{currency}}' WHERE `id` = 32;
UPDATE `notification_templates` SET `email_body` = 'Your deposit of {{amount}} {{currency}} is via  {{method_name}} has been completed Successfully.\r\n<br><br>\r\nDetails of your Deposit :\r\n<br><br>\r\nAmount : {{amount}} {{currency}}<br>\r\nCharge: {{charge}} {{method_currency}}\r\n<br><br>\r\nConversion Rate : 1 {{currency}} = {{rate}} {{method_currency}}<br>\r\nPayable : {{method_amount}} {{method_currency}}<br>\r\nPaid via :  {{method_name}}\r\n<br><br>\r\nTransaction Number : {{trx}}\r\n<br><br>\r\nYour current Balance is {{post_balance}} {{currency}}' WHERE `id` = 31;

ALTER TABLE `support_tickets`
CHANGE `id` `id` bigint(20) unsigned NOT NULL auto_increment COMMENT '',
CHANGE `user_id` `user_id` bigint(20) unsigned NULL DEFAULT NULL COMMENT '';

ALTER TABLE `support_tickets`
ADD COLUMN IF NOT EXISTS `messages` LONGTEXT;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
