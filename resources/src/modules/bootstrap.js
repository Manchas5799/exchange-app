import axios from "axios";
window.axios = axios;

window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

import Echo from "laravel-echo";
import Pusher from "pusher-js";

window.Pusher = Pusher;

window.Echo = new Echo({
    broadcaster: "pusher",
    key: PUSHER_APP_KEY,
    cluster: PUSHER_APP_CLUSTER,
    encrypted: true,
    forceTLS: true,
    authEndpoint: "/broadcasting/auth",
    auth: {
        headers: {
            "X-CSRF-TOKEN": document
                .querySelector('meta[name="csrf-token"]')
                .getAttribute("content"),
        },
    },
});
/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */
// authEndpoint: '/user/pusher/auth?_token={{ csrf_token() }}',
// import Echo from 'laravel-echo';

// import Pusher from 'pusher-js';
// window.Pusher = Pusher;

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: import.meta.env.PUSHER_APP_KEY,
//     wsHost: import.meta.env.PUSHER_HOST ? import.meta.env.PUSHER_HOST : `ws-${import.meta.env.PUSHER_APP_CLUSTER}.pusher.com`,
//     wsPort: import.meta.env.PUSHER_PORT ?? 80,
//     wssPort: import.meta.env.PUSHER_PORT ?? 443,
//     forceTLS: (import.meta.env.PUSHER_SCHEME ?? 'https') === 'https',
//     enabledTransports: ['ws', 'wss'],
// });
