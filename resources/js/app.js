import "./bootstrap";
import "flowbite";

import Alpine from "alpinejs";
import focus from "@alpinejs/focus";
import persist from "@alpinejs/persist";
// import { registerSW } from 'virtual:pwa-register'

// const intervalMS = 24 * 60 * 60 * 1000

// const updateSW = registerSW({
//     onOfflineReady() { },
//     onRegisteredSW(swUrl, r) {
//         r && setInterval(async () => {
//             if (!(!r.installing && navigator))
//                 return

//             if (('connection' in navigator) && !navigator.onLine)
//                 return

//             const resp = await fetch(swUrl, {
//                 cache: 'no-store',
//                 headers: {
//                     'cache': 'no-store',
//                     'cache-control': 'no-cache',
//                 },
//             })

//             if (resp?.status === 200)
//                 await r.update()
//         }, intervalMS)
//     }
// })

window.Alpine = Alpine;

Alpine.plugin(focus);
Alpine.plugin(persist);

Alpine.start();
