@extends('layouts.admin')

@section('content')
    <div class="bg-white dark:bg-gray-800 shadow overflow-hidden sm:rounded-lg">
        <table class="min-w-full divide-y divide-gray-200 dark:divide-gray-700">
            <thead class="bg-gray-50 dark:bg-gray-700">
                <tr>
                    <th scope="col"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">
                        Plan Name
                    </th>
                    <th scope="col"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">
                        Minimum Investment
                    </th>
                    <th scope="col"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">
                        Maximum Investment
                    </th>
                    <th scope="col"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">
                        Duration (in days)
                    </th>
                    <th scope="col"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">
                        Daily Interest Rate (%)
                    </th>
                    <th scope="col"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">
                        Status
                    </th>
                    <th scope="col"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 dark:text-gray-200 uppercase tracking-wider">
                        recommended
                    </th>
                    <th scope="col" class="relative px-6 py-3">
                        <span class="sr-only">Edit</span>
                    </th>
                </tr>
            </thead>
            <tbody class="bg-white dark:bg-gray-800 divide-y divide-gray-200 dark:divide-gray-700">
                @forelse ($plans as $plan)
                    <tr>
                        <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900 dark:text-gray-200">
                            {{ $plan->name }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500 dark:text-gray-300">
                            {{ $plan->min_amount }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500 dark:text-gray-300">
                            {{ $plan->max_amount }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500 dark:text-gray-300">
                            {{ $plan->duration_in_days }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500 dark:text-gray-300">
                            {{ $plan->interest_rate }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                            <input type="checkbox" class="toggle-status" data-id="{{ $plan->id }}" data-toggle="toggle"
                                data-onstyle="success" data-offstyle="danger"
                                @if ($plan->status == 1) checked @endif>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                            <input type="checkbox" class="toggle-recommanded" data-id="{{ $plan->id }}"
                                data-toggle="toggle" data-onstyle="success" data-offstyle="danger"
                                @if ($plan->recommanded == 1) checked @endif>
                        </td>
                        <td class="px-4 py-2 flex space-x-2">
                            <a href="{{ route('admin.investment.plans.edit', $plan->id) }}"
                                class="btn btn-sm btn-outline-warning btn-icon">
                                <i class="bi bi-pencil-square"></i>
                            </a>

                            <form action="{{ route('admin.investment.plans.destroy', $plan->id) }}" method="POST"
                                class="inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-outline-danger btn-icon">
                                    <i class="bi bi-trash"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td class="px-6 py-4">No investment plan found.</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
@endsection

@push('breadcrumb-plugins')
    <a class="btn btn-outline-primary" href="{{ route('admin.investment.plans.create') }}"><i
            class="bi bi-plus-lg"></i>{{ __('Add New') }}</a>
@endpush

@section('page-scripts')
    <script>
        $(document).ready(function() {
            $('.toggle-status').change(function() {
                var id = $(this).data('id');
                var status = $(this).prop('checked');

                $.ajax({
                    url: '/admin/investment/plans/' + id + '/toggle-status',
                    type: 'PUT',
                    data: {
                        _token: '{{ csrf_token() }}',
                        status: status ? 1 : 0
                    },
                    success: function(response) {
                        console.log(response);
                    }
                });
            });
            $('.toggle-recommanded').change(function() {
                var id = $(this).data('id');
                var recommanded = $(this).prop('checked');

                $.ajax({
                    url: '/admin/investment/plans/' + id + '/toggle-recommanded',
                    type: 'PUT',
                    data: {
                        _token: '{{ csrf_token() }}',
                        recommanded: recommanded ? 1 : 0
                    },
                    success: function(response) {
                        console.log(response);
                    }
                });
            });
        });
    </script>
@endsection
