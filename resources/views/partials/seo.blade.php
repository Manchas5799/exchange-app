@if (!empty($seo))
    <title> {{ $general->sitename(__($page_title ?? '')) }}</title>
    <meta name="title" Content="{{ $general->sitename(__($page_title ?? '')) }}">
    <meta name="description" content="{{ $seo->description }}">
    <meta name="keywords" content="{{ implode(',', $seo->keywords) }}">
    <link rel="shortcut icon" id="favicon" href="{{ getImage(imagePath()['logoIcon']['path'] . '/favicon.png') }}"
        type="image/x-icon">

    {{-- <link rel="icon" type="image/png" sizes="32x32"
        href="{{ getImage(imagePath()['logoIcon']['path'] . '/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16"
        href="{{ getImage(imagePath()['logoIcon']['path'] . '/favicon-16x16.png') }}">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <link rel="manifest" href="/build/manifest.webmanifest">
    <link rel="apple-touch-icon" sizes="120x120"
        href="{{ getImage(imagePath()['logoIcon']['path'] . '/apple-touch-icon.png') }}">
    <link rel="mask-icon" href="{{ getImage(imagePath()['logoIcon']['path'] . '/safari-pinned-tab.svg') }}"
        color="#5bbad5"> --}}

    <!-- Apple Stuff -->
    <link rel="apple-touch-icon" href="{{ getImage(imagePath()['logoIcon']['path'] . '/logo.png') }}">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="{{ $general->sitename($page_title ?? '') }}">
    <!-- Google / Search Engine Tags -->
    <meta itemprop="name" content="{{ $general->sitename($page_title ?? '') }}">
    <meta itemprop="description" content="{{ $general->seo_description }}">
    <meta itemprop="image" content="{{ getImage(imagePath()['seo']['path'] . '/' . $seo->image) }}">
    <!-- Facebook Meta Tags -->
    <meta property="og:type" content="website">
    <meta property="og:title" content="{{ $seo->social_title }}">
    <meta property="og:image" content="{{ getImage(imagePath()['seo']['path'] . '/' . $seo->image) }}" />
    <meta property="og:image:type"
        content="image/{{ pathinfo(getImage(imagePath()['seo']['path']) . '/' . $seo->image)['extension'] }}" />
    @php $social_image_size = explode('x', imagePath()['seo']['size']) @endphp
    <meta property="og:image:width" content="{{ $social_image_size[0] }}" />
    <meta property="og:image:height" content="{{ $social_image_size[1] }}" />
    <meta property="og:url" content="{{ url()->current() }}">
    {{-- <!-- Twitter Meta Tags --> --}}
    <meta name="twitter:card" content="summary_large_image">
@endif
